"""
<main>
<main script for data analysis>

Author: Vincenzo DI Capua <vincenzo.di.capua@cern.ch>
License: GNU General Public License v3.0 (GPL-3.0)
"""

def main():
    # Your main script logic goes here
    pass

if __name__ == "__main__":
    # Argument parser setup
    parser = argparse.ArgumentParser(description="<Description of your script>")

    # Add command-line arguments
    parser.add_argument("--input", help="Input file path", required=True)
    parser.add_argument("--output", help="Output file path", default="output.txt")

    # Parse command-line arguments
    args = parser.parse_args()

    # Access parsed arguments
    input_path = args.input
    output_path = args.output

    # Call the main function
    main()
