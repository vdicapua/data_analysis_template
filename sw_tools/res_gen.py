import numpy as np  
import os
import matplotlib.pyplot as plt 
import git 
def res_gen (choice, data, markdown_text,engineer, save_path="."):
    # Create folder named Result_<choice>
    folder_name = f"Result_{choice}"
    folder_path = os.path.join(save_path, folder_name)
    os.makedirs(folder_path, exist_ok=True)

    # Create README_<choice>.md file
    md_file_path = os.path.join(folder_path, f"README_{choice}.md")

    with open(md_file_path, "w") as md_file:
        
        # Git repository information
        try:
            repo = git.Repo(search_parent_directories=True)
            user_name = repo.head.commit.author.name
            user_email = repo.head.commit.author.email
            md_file.write("This folder contained some result considered interesting by: " )
            md_file.write(f"User: {user_name} <{user_email}>\n")
            md_file.write("#CONTENT " + "\n\n")
            md_file.write(markdown_text + "\n\n")
            md_file.write(f"Repository: {repo.remotes.origin.url}\n")
            md_file.write(f"Branch: {repo.active_branch}\n")
            md_file.write(f"Commit: {repo.head.commit}\n\n")
        except git.InvalidGitRepositoryError:
            md_file.write("This folder contained some result considered interesting by: " )
            md_file.write(f"User: "+engineer+"\n")
            md_file.write("#CONTENT " + "\n\n")
            md_file.write(markdown_text + "\n\n")
            md_file.write("Git repository not found\n\n")

        # Additional text provided by user
  



    # Create plot in PDF
    pdf_plot_path = os.path.join(folder_path, f"result_{choice}.pdf")
    plt.figure()
    plt.plot(data[0],data[1])  # Assuming data is a list of values to plot
    plt.savefig(pdf_plot_path)
    plt.close()

    # Create text file with input data
    txt_file_path = os.path.join(folder_path, f"input_data_{choice}.txt")
    np.savetxt(txt_file_path, np.column_stack((data[0], data[1])), delimiter=" ", comments="")
